﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PassGenerator
{
    class crypt
    {
        private const int p = 3719;
        private const int q = 3907;
        private const int z = p*q;
        private const int phi = (p - 1) * (q - 1);
        private const int n = 1951;
        private const int s = 13279423;



        private static int findS(int n, int phi)
        {
            var x = 1;
            while (x < (z - 1))
            {
              
                if ((n * x) % phi == 1)
                {
                    return x;
                }
                x++;
            }
            return 0;
        }

        private static double expoMod(double a,int n, int z)
        {
            double i = n;
            double r = 1;
            double x = a % z;
 
            while (i > 0)
            {
                if (i % 2 == 1)
                {
                    r = (r * x) % z;
                }
                x = (x * x) % z;
                i = Math.Floor(i / 2);
                
            }
            System.Diagnostics.Debug.WriteLine("r");
            System.Diagnostics.Debug.WriteLine(r);
            return r;
        }

        public static string encrypt(string message)
        {
            string mes = "";
            for (var i = 0; i < message.Length; i++)
            {
                var uni = message[i];
                mes = String.Concat(mes,expoMod(uni, n, z));
                mes += '-';
            }
            return mes;
        }

        public static string decrypt(string coded)
        {
            var mes = "";
            double index = 0;
            var balise = 0;
            for(var i = 0; i < coded.Length; i++)
            {
                if (coded[i]=='-')
                {
                    index = double.Parse(coded.Substring(balise, i-balise));                  
                    var decoded = expoMod(index, s, z);
                    mes += Convert.ToChar((int)decoded);                  
                    balise = i+1;                                      
                }
                
            }
            return mes;
        }
    }
}
