﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PassGenerator
{
    public partial class Form1 : Form
    {
        password all = new password();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string place = passGen.Text;
            if (length.Text == "")
            {
                changeAlertLabel("please enter length");
                return;
            }
            int passLength = Int32.Parse(length.Text);

            
            all.generatePassword(place, passLength, this);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void getButton_Click(object sender, EventArgs e)
        {
            string wantPass = passGet.Text;

            if (wantPass == "")
            {
                changeAlertLabel("please enter a the place you want the password");
                return;
            }
            all.getPassword(wantPass,this);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public void changeAlertLabel(string message)
        {
            alertLabel.Text = message;
        }

        public void changeTextBox1(string message)
        {
            textBox1.Text = message;
        }

        private void passGet_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            System.Windows.Forms.Clipboard.SetText(textBox1.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {

            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string key = modText1.Text;
            string value = modText2.Text;

            if (all.searchKey(key))
            {
                all.modify(key, value);
                changeAlertLabel("pasword is modified");
            }
            else
            {
                changeAlertLabel("Key is not found");
            }
            
        }
    }
}
