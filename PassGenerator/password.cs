﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace PassGenerator
{ 
    class password
    {       
        private Dictionary<string, string> all_pass;
        private string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

        private void load()
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(path+"/pass.bin",
                                      FileMode.Open,
                                      FileAccess.Read,
                                      FileShare.Read);
            all_pass = (Dictionary<string, string>)formatter.Deserialize(stream);
            stream.Close();
        }

        private void save()
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(path + "/pass.bin",
                                     FileMode.Create,
                                     FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, all_pass);
            stream.Close();
        }
        
        public Boolean searchKey(string key)
        {
            if (all_pass.ContainsKey(key))
            {
                return true;              
            }
            else
            {
                return false;
            }
            
        }

        public void modify(string key, string value)
        {
            all_pass[key] = crypt.encrypt(value);
            save();
        }

        public void generatePassword(string place, int length, Form1 f)
        {
  
            if (File.Exists(path + "/pass.bin"))
            {
                load();
            }
            

            if (all_pass == null)
            {
                all_pass = new Dictionary<string, string>();
            }
            
            if (all_pass.ContainsKey(place))
            {
                f.changeAlertLabel("Password already generated");
                f.changeTextBox1(crypt.decrypt(all_pass[place]));
                return;
            }
            var pass = "";

            Random rnd = new Random();
            for (var i = 0; i < length; i++)
            {              
                pass += Convert.ToChar(rnd.Next(48, 123));
            }
            all_pass[place] = crypt.encrypt(pass);
            f.changeAlertLabel("Password is: ");
            f.changeTextBox1(pass);

            save();
        }

        public void getPassword(string p, Form1 f)
        {

            load();

            if (!all_pass.ContainsKey(p))
            {
                f.changeAlertLabel("Password does not exist");
                f.changeTextBox1("");
                return;
            }
            //f.changeTextBox1(all_pass[p]);

            f.changeTextBox1(crypt.decrypt(all_pass[p]));
        }
    }
}
